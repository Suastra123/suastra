class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :nama
      t.string :email
      t.string :telp
      t.string :alamat

      t.timestamps
    end
  end
end
